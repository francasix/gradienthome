# Gradient homepage / personnal website / switching gradient background

## Start server on port 9000

`node server.js`

## Start watcher

`yarn start`

## Tests

`yarn test`

## Help / Thanks

* [https://codepen.io/thebabydino/pen/hkxGp]()https://codepe.io/thebabydino/pen/hkxGp)

* [https://webgradients.com/](https://webgradients.com/)

* [https://codepen.io/yesilfasulye/pen/iGHru](https://codepen.io/yesilfasulye/pen/iGHru)

* [https://blogs.infinitesquare.com/posts/web/debuter-un-projet-avec-vuejs-typescript-et-webpack](https://blogs.infinitesquare.com/posts/web/debuter-un-projet-avec-vuejs-typescript-et-webpack)

### Screen Console 

![console ok](https://bitbucket.org/francasix/gradienthome/raw/0a4ea3e8b74834447a882fa61c71f2eac8362440/images/readme-screenshot.png)

## Deploy

Copy /dist content to server

```bash
scp dist/build.js hostname@host:/my/path/to/server/files
```
