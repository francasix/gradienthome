const express = require('express');

const app = express();

app.get('/', function (request, response) {
    response.sendFile(__dirname + "/index.html");
    app.use("/dist", express.static(__dirname + '/dist'));
});
app.listen(9000, ()=>{
    console.log('==== App served port 9000 ==== ');
});
