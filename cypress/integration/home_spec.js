describe('Homepage', () => {

    beforeEach(() => {
        cy.visit('http://localhost:9000/')
    });

    it('Homepage is ok', () => {
        cy.get('.email').contains('paulinegc@zoho.com')
    });

    it('User change the background', () => {
        updateGradient();
    });
})

const updateGradient = () => {
    cy.get('.button').click()
}