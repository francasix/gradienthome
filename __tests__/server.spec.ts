import chai from 'chai';
import chaiHttp from 'chai-http';
import 'mocha';

chai.use(chaiHttp);
const expect = chai.expect;

describe('Server runs', () => {
	it('should return response 200 on call', () => {

		return chai.request('http://localhost:9000').get('/')
			.then(res => {
				chai.expect(res.status).to.eql(200);
			})
	})
})