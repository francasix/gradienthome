export interface Gradient {

	colorA: string;
	colorB?: string;

}