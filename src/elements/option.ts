import {Gradient} from './Gradient';

export interface Option {

	id: number;
	gradient: Gradient;
	target: string;

}

