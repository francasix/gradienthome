/**
 * Extend prototype
 * from http://mcgivery.com/htmlelement-pseudostyle-settingmodifying-before-and-after-in-javascript/
 */
export {}

declare global {
	interface HTMLElement {
		pseudoStyle(element: string, prop: string, value: string): object;
	}
}

HTMLElement.prototype.pseudoStyle = function(element,prop,value){

	let _this = this,
		_sheetId = "pseudoStyles",
		_head = document.head || document.getElementsByTagName('head')[0],
		_sheet = document.getElementById(_sheetId) || document.createElement('style');

	_sheet.id = _sheetId;

	_sheet.innerHTML += "#"+_this.id+":"+element+"{"+prop+":"+value+"}";

	_head.appendChild(_sheet);

	return this;
};
