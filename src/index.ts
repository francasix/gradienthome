import Vue from "vue";
import Home from "./components/home.component";

let v = new Vue({
	el: "#app",
	template: `<div><Home /></div>`,
	components: {Home}
});