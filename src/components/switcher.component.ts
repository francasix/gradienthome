import { Vue, Component } from 'vue-property-decorator';

@Component({
	template: require('./switcher.component.html'),
})
export default class Switcher extends Vue {
	constructor(){
		super();
	}
	switchGradient(){
		this.$emit("switch-gradient");
	}
}