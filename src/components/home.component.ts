import {Component, Emit, Vue} from 'vue-property-decorator';
import switcher               from './switcher.component';
import {Option}               from '../elements/option';
import './../helper.extensions';
import data                   from './../../data/colors.json';

@Component({
	template: require('./home.component.html'),
	components: {
		switcher
	}
})
export default class Home extends Vue {

	beforeOption: Option = {
		id: 0,
		gradient: {
			colorA: '#fa5',
			colorB: '#32dda2'
		},
		target: 'before'
	};
	afterOption: Option = {
		id: 1,
		gradient: {
			colorA: '#f55',
			colorB: '#fa89e7'
		},
		target: 'after'
	};

	options: Option[] = [this.beforeOption, this.afterOption];
	currentOption: Option = this.beforeOption;

	private _containerElement;

	// tuple
	duoHexadecimal: [string, string];

	mounted() {
		this._containerElement = <HTMLElement> document.getElementById('main-container');
	}

	@Emit()
	async switchGradient() {

		await this.updateCurrentOption();

		await this.updateGradient(data['colors']);

		// update gradient hidden element --> for next background's change
		this._containerElement.pseudoStyle(this.currentOption.target, 'background-image', `linear-gradient(${this.duoHexadecimal[0]}, ${this.duoHexadecimal[1]})`);

	}

	/**
	 * gradient with check duo colors are different
	 */
	updateGradient(colors: object[]) {

		this.duoHexadecimal = [this.fetchHexadecimal(colors), ""];

		do {
			this.duoHexadecimal[1] = this.fetchHexadecimal(colors);
		} while (this.duoHexadecimal[0] == this.duoHexadecimal[1]);

		return this.duoHexadecimal;

	}

	/**
	 * @return string
	 */
	fetchHexadecimal(data: object[]) {

		let random = Math.floor(Math.random() * Math.floor(data.length));

		return data[random]['hexadecimal'];
	}

	/**
	 * reverse current active option before/after and
	 * wait for css transition ended
	 */
	updateCurrentOption() {

		return new Promise<any>(
			(resolve, reject) => {

				this.currentOption = this.options[this.currentOption.id == 0 ? 1 : 0];

				this._containerElement.pseudoStyle(this.currentOption.target, 'opacity', '0');
				this._containerElement.pseudoStyle(this.options[this.currentOption.id == 0 ? 1 : 0].target, 'opacity', '1');


				setTimeout(() => {

					resolve();

				}, 1000);
			});
	}

}